/*
 * @Author: your name
 * @Date: 2020-09-12 18:28:05
 * @LastEditTime: 2020-09-13 17:11:50
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /vue-ant-admin/src/layout/components/menus.ts
 */
import { Menu } from 'ant-design-vue'
import { defineComponent, ref, watch } from 'vue'
import { useStore } from 'vuex'
import { State } from '@/store'
import { useRouter, useRoute } from 'vue-router'

type routesType = State['permission']['routes']
type routeType = routesType[number]

function getMenuItemById(menus: routesType, id: string): routeType | undefined {
  for (const menu of menus) {
    if (menu.id === id) {
      return menu
    } else if (menu.children?.length) {
      const findMenu: routeType | undefined = getMenuItemById(menu.children, id)
      if (findMenu) {
        return findMenu
      }
    }
  }
}

function getMenuItemByPath(
  menus: routesType,
  path: string
): routeType | undefined {
  for (const menu of menus) {
    if (menu.path === path) {
      return menu
    } else if (menu.children?.length) {
      const findMenu: routeType | undefined = getMenuItemByPath(
        menu.children,
        path
      )
      if (findMenu) {
        return findMenu
      }
    }
  }
}

export default defineComponent({
  setup() {
    const store = useStore<State>()
    const router = useRouter()
    const menus: routesType = store.getters['permission/getRoleMenus']
    const selectedKeys = ref<string[]>([])

    function handleSelect(res: any) {
      selectedKeys.value = res.selectedKeys
      const id = selectedKeys.value[0]
      const currentMenu = getMenuItemById(menus, id)
      if (currentMenu && currentMenu.path) {
        router.push(currentMenu.path)
      }
    }

    const route = useRoute()
    watch(
      () => route,
      () => {
        const menu = getMenuItemByPath(menus, route.path)
        if (menu) {
          selectedKeys.value = [menu.id]
        }
      },
      {
        immediate: true,
        deep: true
      }
    )

    function renderMenu(menu: any) {
      if (menu.children?.length) {
        return (
          <Menu.SubMenu title={menu.text} key={menu.id}>
            {menu.children.map(renderMenu)}
          </Menu.SubMenu>
        )
      }
      return <Menu.Item key={menu.id}>{menu.text}</Menu.Item>
    }

    return () => {
      return (
        <Menu
          theme="dark"
          mode="inline"
          selectedKeys={selectedKeys.value}
          onSelect={handleSelect}
          inlineCollapsed={false}
          multiple={false}
        >
          {menus?.map(renderMenu)}
        </Menu>
      )
    }
  }
})
