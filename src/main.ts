/*
 * @Author: your name
 * @Date: 2020-09-10 10:10:51
 * @LastEditTime: 2020-09-13 12:41:50
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /vue-ant-admin/src/main.ts
 */
import { createApp } from 'vue'
import Antd from 'ant-design-vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './permission'
import 'ant-design-vue/dist/antd.css'

const app = createApp(App)
  .use(store)
  .use(router)

app.use(Antd)

app.mount('#app')
