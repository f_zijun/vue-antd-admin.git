/*
 * @Author: your name
 * @Date: 2020-09-10 10:10:51
 * @LastEditTime: 2020-09-13 12:54:02
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /vue-ant-admin/src/store/index.ts
 */
import { createStore } from 'vuex'
import permission, { State as PermissionState } from './modules/permission'

export interface State {
  permission: PermissionState
}

export default createStore({
  modules: {
    permission
  },
  state: {},
  mutations: {},
  actions: {}
})
