/*
 * @Author: your name
 * @Date: 2020-09-13 12:51:58
 * @LastEditTime: 2020-09-13 16:47:02
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /vue-ant-admin/src/store/data/permission.js
 */
export default [
  {
    text: '首页',
    id: 'page1',
    path: '/home'
  },
  {
    text: '关于',
    id: 'page2',
    path: '/about'
  }
]
