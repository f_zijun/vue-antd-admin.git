import { ActionContext } from 'vuex'
import data from '../data/permission'

/*
 * @Author: your name
 * @Date: 2020-09-12 17:01:43
 * @LastEditTime: 2020-09-13 14:24:22
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /vue-ant-admin/src/store/permission.ts
 */

export interface Route {
  text: string
  id: string
  path?: string
  children?: Array<Route>
}

export interface State {
  routes: Route[]
  addRoutes: Route[]
}

const state: State = {
  routes: [],
  addRoutes: []
}

const mutations = {
  SET_ROUTES(state: State, routes: Route[]) {
    state.routes = routes
  }
}

const actions = {
  loadRoleMenus({ commit }: ActionContext<State, any>) {
    return new Promise(resolve => {
      commit('SET_ROUTES', data)
      resolve(data)
    })
  }
}

const getters = {
  getRoleMenus: (state: State) => {
    return state.routes
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
